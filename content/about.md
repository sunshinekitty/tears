---
title: About
---

My name is Alex.  This is my blog full of tech rants and tutorials, I hope you find some of this information useful.

##### You may also find me these places

- <a href="https://www.instagram.com/sunshinekitty5" target="_blank">Instagram</a> 📷
- <a href="https://twitter.com/sunshinekitty5" target="_blank">Twitter</a> 🐦
- <a href="https://github.com/sunshinekitty" target="_blank">Github</a> 💻

##### If you like tech blogs maybe check out my friends

- <a href="https://suchprogramming.com/" target="_blank">Such Programming</a>
