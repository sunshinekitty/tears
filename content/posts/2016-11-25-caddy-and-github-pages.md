---
date: 2016-11-25
comments: true
title: Caddy and Github Pages
description: It was not immediately obvious to me on how to setup Caddy as a proxy to Github Pages, this was mostly due to a misunderstanding on how Github Pages work, but thought I’d share a quick guide to hopefully clear up any questions.
---

It was not immediately obvious to me on how to setup Caddy as a proxy to Github Pages, this was mostly due to a misunderstanding on how Github Pages work, but thought I'd share a quick guide to hopefully clear up any questions.

## Configuring Github Pages

I won't go into much detail here, a good guide to get started is located [here](https://guides.github.com/features/pages/).  What's important is to remember what you set your **Custom domain** as.  This will be the site you need to serve via Caddy.  In this example my **Custom domain** is set as **tears.io**.

## Configure Caddy

```bash
tears.io {
    header / {
        # Enable HTTP Strict Transport Security (HSTS) to force clients to always connect via HTTPS
        Strict-Transport-Security "max-age=31536000;"
        # Enable cross-site filter (XSS) and tell browser to block detected attacks
        X-XSS-Protection "1; mode=block"
        # Prevent some browsers from MIME-sniffing a response away from the declared Content-Type
        X-Content-Type-Options "nosniff"
        # Disallow the site to be rendered within a frame (clickjacking protection)
        X-Frame-Options "DENY"
    }
    proxy / http://sunshinekitty.github.io/ {
        transparent
    }
    log /var/log/caddy/tears.io-access.log
    errors /var/log/caddy/tears.io-errors.log
    gzip
}
```

The transparent setting in the proxy directive is short-hand for:

```
header_upstream Host {host}
header_upstream X-Real-IP {remote}
header_upstream X-Forwarded-For {remote}
header_upstream X-Forwarded-Proto {scheme}
```

Note that the proxy directive is pointing to my main Github Pages page, and not to the sub-page of `/tears.io`.  This is on purpose, Github Pages knows to serve the normal site as long as the `Host` header is set to `tears.io` in this instance.

That wraps things up, this shows how this blog is hosted from Github Pages at [github.com/sunshinekitty/tears.io](https://github.com/sunshinekitty/tears.io).

> Addendum: Now in 2019 I am hosting my website from Gitlab pages with Cloudflare providing SSL.  I will stick with this for the time being, if Gitlab does something to betray my trust I could see moving to something like GCS and Cloud Builder in GCP.

